﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMove : MonoBehaviour
{
    [Header("Movement")]
    public float speedMax;
    private float speed;
    public float jumpForce;
    private Vector3 moveDirection;
    public float gravity;
    public float rotationSpeed;

    [Header("Camera")]
    public bool tps;
    public Transform head;
    public Transform back;
    private CharacterController controller;

    void Start()
    {
        controller = GetComponent<CharacterController>();
        ChangeViewMode();
    }

    void Update()
    {
        var strafe = Input.GetAxis("Horizontal");
        var forward = Input.GetAxis("Vertical");

        var mouseX = Input.GetAxis("Mouse X");
        var mouseY = Input.GetAxis("Mouse Y");

        // Direction forward
        speed = forward * speedMax * Time.deltaTime;
        moveDirection = transform.forward * speed;

        // Side direction
        strafe *= speedMax * Time.deltaTime; 
        moveDirection += transform.right * strafe;

        var jump = Input.GetButton("Jump");

        if (jump) moveDirection.y = jumpForce * Time.deltaTime;
        else moveDirection.y = gravity * Time.deltaTime;

        var rotHorizontal = new Vector3(0f, mouseX, 0f);
        var rotVertical = new Vector3(-mouseY, 0f, 0f);
        transform.Rotate(rotHorizontal * rotationSpeed * Time.deltaTime);
        head.Rotate(rotVertical * rotationSpeed * Time.deltaTime);


        controller.Move(moveDirection);

        // Switch FPS & TPS view
        if (Input.GetButtonDown("Fire1"))
        {
            ChangeViewMode();
        }

    }

    void ChangeViewMode()
    {
        if (tps)
        {
            Camera.main.transform.parent = head;
            Camera.main.transform.localPosition = Vector3.zero;
            tps = false;
        }
        else
        {
            Camera.main.transform.parent = back;
            Camera.main.transform.localPosition = Vector3.zero;
            tps = true;
        }
    }
}
